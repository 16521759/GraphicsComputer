#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;

//Check if mouse is in button
bool mouseClick(SDL_Rect *rect, int x, int y)
{
	if ((x >= rect->x) && (x <= (rect->x + rect->w)) && (y >= rect->y) && (y <= (rect->y + rect->h)))
		return true;
	else
		return false;
}
int main(int, char**) 
{
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);

	SDL_SetRenderDrawColor(ren, 150, 255, 0, 255);
	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
	SDL_RenderDrawLine(ren, p2.x, p2.y, p3.x, p3.y);
	SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);

	//DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);

	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 5;
	rect1->y = p1.y - 5;
	rect1->w = 10;
	rect1->h = 10;
	SDL_RenderDrawRect(ren, rect1);
	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 5;
	rect2->y = p2.y - 5;
	rect2->w = 10;
	rect2->h = 10;
	SDL_RenderDrawRect(ren, rect2);
	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 5;
	rect3->y = p3.y - 5;
	rect3->w = 10;
	rect3->h = 10;
	SDL_RenderDrawRect(ren, rect3);
	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 5;
	rect4->y = p4.y - 5;
	rect4->w = 10;
	rect4->h = 10;
	SDL_RenderDrawRect(ren, rect4);
	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
	
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;

	while (running)
	{
		//If there's events to handle
		while (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				bool runningTemp = true;
				while (runningTemp)
				{
					SDL_Event e;
					while (SDL_PollEvent(&e))
					{
						if (e.type == SDL_MOUSEBUTTONUP)
						{
							runningTemp = false;
						}
						if (e.type == SDL_MOUSEMOTION)
						{
							//Get mouse position
							int x, y;
							SDL_GetMouseState(&x, &y);
							Vector2D new_p;
							new_p.x = x;
							new_p.y = y;
							//Check if mouse is in button
							if (mouseClick(rect1, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								SDL_SetRenderDrawColor(ren, 150, 255, 0, 255);
								SDL_RenderDrawLine(ren, new_p.x, new_p.y, p2.x, p2.y);
								SDL_RenderDrawLine(ren, p2.x, p2.y, p3.x, p3.y);
								SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);
								p1 = new_p;
								DrawCurve3(ren, p1, p2, p3, p4);
								rect1->x = new_p.x - 5;
								rect1->y = new_p.y - 5;
								rect1->w = 10;
								rect1->h = 10;
								SDL_RenderDrawRect(ren, rect1);
								SDL_RenderDrawRect(ren, rect2);
								SDL_RenderDrawRect(ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (mouseClick(rect2, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								SDL_SetRenderDrawColor(ren, 150, 255, 0, 255);
								SDL_RenderDrawLine(ren, p1.x, p1.y, new_p.x, new_p.y);
								SDL_RenderDrawLine(ren, new_p.x, new_p.y, p3.x, p3.y);
								SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);
								p2 = new_p;
								DrawCurve3(ren, p1, p2, p3, p4);
								rect2->x = new_p.x - 5;
								rect2->y = new_p.y - 5;
								rect2->w = 10;
								rect2->h = 10;

								SDL_RenderDrawRect(ren, rect1);
								SDL_RenderDrawRect(ren, rect2);
								SDL_RenderDrawRect(ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (mouseClick(rect3, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								SDL_SetRenderDrawColor(ren, 150, 255, 0, 255);
								SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
								SDL_RenderDrawLine(ren, p2.x, p2.y, new_p.x, new_p.y);
								SDL_RenderDrawLine(ren, new_p.x, new_p.y, p4.x, p4.y);
								p3 = new_p;
								DrawCurve3(ren, p1, p2, p3, p4);
								rect3->x = x - 5;
								rect3->y = y - 5;
								rect3->w = 10;
								rect3->h = 10;
								SDL_RenderDrawRect(ren, rect1);
								SDL_RenderDrawRect(ren, rect2);
								SDL_RenderDrawRect(ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
							if (mouseClick(rect4, x, y))
							{
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								SDL_SetRenderDrawColor(ren, 150, 255, 0, 255);
								SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
								SDL_RenderDrawLine(ren, p2.x, p2.y, p3.x, p3.y);
								SDL_RenderDrawLine(ren, p3.x, p3.y, new_p.x, new_p.y);
								p4 = new_p;
								DrawCurve3(ren, p1, p2, p3, p4);
								rect4->x = x - 5;
								rect4->y = y - 5;
								rect4->w = 10;
								rect4->h = 10;
								SDL_RenderDrawRect(ren, rect1);
								SDL_RenderDrawRect(ren, rect2);
								SDL_RenderDrawRect(ren, rect3);
								SDL_RenderDrawRect(ren, rect4);
								SDL_RenderPresent(ren);
							}
						}
					}// End Event e
				} // End runningTemp
			} // End if

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
			
		}// End Event event

	}// End running

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();


	return 0;
}
