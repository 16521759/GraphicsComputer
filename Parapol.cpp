#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	// Points 1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 2
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0, y = 0;
	int p;
	// Area 1
	p = 1 - A;
	while (x <= A)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y = y + 1;
		}
		x = x + 1;
	}
	// Area 2
	p = 2 * A - 1;
	while (y <= yc)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x = x + 1;
		}
		y = y + 1;
	}

}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0, y = 0;
	int p;
	// Area 1
	p = 1 - A;
	while (x >= -A)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += -2 * x + 3;
		}
		else
		{
			p += -2 * x + 3 - 2 * A;
			y = y - 1;
		}
		x = x - 1;
	}
	// Area 2
	p = 2 * A - 1;
	while (y >= -yc)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A + 4 * x - 4;
			x = x - 1;
		}
		y = y - 1;
	}
}
